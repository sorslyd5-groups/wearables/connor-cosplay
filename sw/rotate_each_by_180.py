import pcbnew
import os

#board = pcbnew.GetBoard()
#board = pcbnew.LoadBoard('ringLed.kicad_pcb')
board = pcbnew.LoadBoard('C:\\Users\\Sorsly\\Documents\\remote\\connor\\ecad\\ringLed\\ringLed.kicad_pcb')

modules = [
        "D1",
        "D2",
        "D3",
        "D4",
        "D5",
        "D6",
        "D7",
        "D8",
        "D9",
        "D10",
        "D11",
        "D12",
        "D13",
        "D14",
        "D15",
        "D16"
        ]

# for loop iterate over list of module desginators
for i in modules:
    # find module by reference
    mod = board.FindModuleByReference(i)
    
    # rotate module by 180
    print("old angle:", mod.GetOrientation())
    curr_a = mod.GetOrientation()
    next_a = curr_a/10 + 180  #reading angles in kicad are to the tenth place, but SETTING IS to the ONES place???
    print("next_a:",next_a)
    mod.SetOrientationDegrees(next_a)
    print("new angle:", mod.GetOrientation())
    print("")

board.Save('C:\\Users\\Sorsly\\Documents\\remote\\connor\\ecad\\ringLed\\autogen.kicad_pcb')
