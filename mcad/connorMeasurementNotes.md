irl eyeball is approx 25mm, +- 1mm from human to human. low variation
markus eyeball model default import scale shows 0.0363m

# ring
od: 0.0235m
id: 0.0145m

od:
25mm/0.0363 = x/0.0235
od x = 16.18mm

id:
25mm/0.0363 = x/0.0145
id x = 9.986mm

thickness = 16.18 - 9.986 = 6.194mm

# triangle
## front
default import for kara model
ring od, kara: 0.015665m
ring od, irl: 16mm
sides of triangle (avg): 0.065
height of triangle (avg): 0.056

sides:
16mm/0.015665 = x/0.065
x = 66.39mm
derived height: (x/2 * sqrt(3)) = 57.50mm

height:
16mm/0.015665 = x/0.056
x = 57.2mm

board is ~ 66.5 x 57.5mm

sidenote: just to check work, checked eyeball mm dimension using same method
25.53mm, pretty consistent

## back triangle
sides of triangle (avg): 0.0709
height of triangle (avg): 0.0622

sides:
16mm/0.015665 = x/0.0709
x = 72.42mm
derived height: 62.72mm

height:
16mm/0.015665 = x/0.0622
x = 63.53mm

board is ~ 72.5mm x 63.5mm

didn't realize that the triangles were different, but suspected
may potentially reuse pcb, but resize enclosure to accomodate larger footprint w/o having variant pcb

# armband
using connor model for this (found on deviantart, lol)

ring od, connor: 0.046975m
eyeball, connor: 0.074377m

armband height: 
0.193
armband dia1: 
0.3088
armband dia2: 
0.37

diameters were measured on shoulder side, not hand. elliptical cylinder

armband height, calculated:
16mm/0.046975 = x/0.193
x = 65.737mm

armband dia1, calc:
16mm/0.046975 = x/0.3088
x = 105.18mm

armband dia2, calc:
16mm/0.046975 = x/0.37
x = 126.02mm

ellipse, calc:
a = 105.18mm/2 = 52.59mm
b = 126.02mm/2 = 63.01mm
...
circumference = 363.906mm

area = 365mm x 65mm
width can be adjusted to fit my own arm, vs connor's
