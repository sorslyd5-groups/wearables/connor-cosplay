# meta

connor cosplay

# todo

- [x] need to create a script that rotates all of the components by 180 deg

# ringLED

multiple modes of operation, changed to by pressing a button on the hmi

## something you've never seen before

- blue pulsing & spinning rings (empty spaces, 2 or 3)
    - min is dim but still on, high is bright. pulsing is steady
    - spinning counterclockwise
    - actually, empty spaces seem to pop in and out
    - spinning appears to be at minimum 2 incomplete rings spinning on top one another
- yellow mode is preceded by blue mode pulsing in increased frequency
    - after switching to yellow from blue, yellow still has blues pattern for a few (2, 3) pulses
    - main yellow is erratic blinking of entire led strip
    - feels like the led is blinking rapidly; drop off is instant but turn on is sometimes gradual
- transition back to blue shows a little blinking
    - so it looks like transitions bleed, whatever mode it is

## 28 stab wounds

- yellow mode actually has all the marks of blue mode as described in "something you've never seen before", but is yellow
    - looks like foreground ring spins slower than background ring, not sure if specific to yellow or also prevalent in blue
- red mode is actually yellow from the previous mode as described in "something you've never seen before", but is red
    - transition was near instant from yellow to red
    - red mode actually has foreground ring spinning in both ccw and cw directions
- transition back from red to yellow was interesting
    - looks like a wipe transition counter clockwise around the ring from the 12 oclock position




