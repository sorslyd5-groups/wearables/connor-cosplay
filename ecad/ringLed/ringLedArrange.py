from kicad.pcbnew import Board
import math

editor_board = Board.from_editor()

# inputs
numLeds = 16    # number of leds
outerd = 16     # outer diameter of circle in mm
innerd = 10     # inner diameter of circle in mm
ledP = 1.5      # led pitch (dimension of square led) in mm

# load footprints
# need to find code that makes sense TODO

# calc
# angle step for orientation of leds
ang_step = 360/numLeds
# radius from origin
rad = ((outerd + innerd)/2)/2   # radius of avg circle
# x,y using polar coords of leds
for i in range(0,numLeds):
    theta = i * ang_step
    x = rad * cos(theta)
    y = rad * sin(theta)
    print("x: ", x, "y: ", y)
    

