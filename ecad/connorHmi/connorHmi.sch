EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Switch:SW_Push SW1
U 1 1 612AD801
P 5550 3450
F 0 "SW1" V 5504 3598 50  0000 L CNN
F 1 "SW_Push" V 5595 3598 50  0000 L CNN
F 2 "Button_Switch_SMD:SW_SPST_B3S-1000" H 5550 3650 50  0001 C CNN
F 3 "~" H 5550 3650 50  0001 C CNN
	1    5550 3450
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 612ADDF3
P 5550 4150
F 0 "#PWR0101" H 5550 3900 50  0001 C CNN
F 1 "GND" H 5555 3977 50  0000 C CNN
F 2 "" H 5550 4150 50  0001 C CNN
F 3 "" H 5550 4150 50  0001 C CNN
	1    5550 4150
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0102
U 1 1 612ADFF6
P 5550 3150
F 0 "#PWR0102" H 5550 3000 50  0001 C CNN
F 1 "VCC" H 5567 3323 50  0000 C CNN
F 2 "" H 5550 3150 50  0001 C CNN
F 3 "" H 5550 3150 50  0001 C CNN
	1    5550 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 3650 5550 3700
Wire Wire Line
	5550 4050 5550 4150
Wire Wire Line
	5550 3700 5200 3700
Connection ~ 5550 3700
Wire Wire Line
	5550 3700 5550 3750
Wire Wire Line
	5550 3150 5550 3250
Text Label 5200 3700 0    50   ~ 0
pb0
$Comp
L Device:R R1
U 1 1 612ADD28
P 5550 3900
F 0 "R1" H 5620 3946 50  0000 L CNN
F 1 "10k" H 5620 3855 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5480 3900 50  0001 C CNN
F 3 "~" H 5550 3900 50  0001 C CNN
	1    5550 3900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW2
U 1 1 612B28F2
P 6450 3450
F 0 "SW2" V 6404 3598 50  0000 L CNN
F 1 "SW_Push" V 6495 3598 50  0000 L CNN
F 2 "Button_Switch_SMD:SW_SPST_B3S-1000" H 6450 3650 50  0001 C CNN
F 3 "~" H 6450 3650 50  0001 C CNN
	1    6450 3450
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 612B28F8
P 6450 4150
F 0 "#PWR0103" H 6450 3900 50  0001 C CNN
F 1 "GND" H 6455 3977 50  0000 C CNN
F 2 "" H 6450 4150 50  0001 C CNN
F 3 "" H 6450 4150 50  0001 C CNN
	1    6450 4150
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0104
U 1 1 612B28FE
P 6450 3150
F 0 "#PWR0104" H 6450 3000 50  0001 C CNN
F 1 "VCC" H 6467 3323 50  0000 C CNN
F 2 "" H 6450 3150 50  0001 C CNN
F 3 "" H 6450 3150 50  0001 C CNN
	1    6450 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 3650 6450 3700
Wire Wire Line
	6450 4050 6450 4150
Wire Wire Line
	6450 3700 6100 3700
Connection ~ 6450 3700
Wire Wire Line
	6450 3700 6450 3750
Wire Wire Line
	6450 3150 6450 3250
Text Label 6100 3700 0    50   ~ 0
pb1
$Comp
L Device:R R2
U 1 1 612B290B
P 6450 3900
F 0 "R2" H 6520 3946 50  0000 L CNN
F 1 "10k" H 6520 3855 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6380 3900 50  0001 C CNN
F 3 "~" H 6450 3900 50  0001 C CNN
	1    6450 3900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW3
U 1 1 612B35CA
P 7350 3450
F 0 "SW3" V 7304 3598 50  0000 L CNN
F 1 "SW_Push" V 7395 3598 50  0000 L CNN
F 2 "Button_Switch_SMD:SW_SPST_B3S-1000" H 7350 3650 50  0001 C CNN
F 3 "~" H 7350 3650 50  0001 C CNN
	1    7350 3450
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 612B35D0
P 7350 4150
F 0 "#PWR0105" H 7350 3900 50  0001 C CNN
F 1 "GND" H 7355 3977 50  0000 C CNN
F 2 "" H 7350 4150 50  0001 C CNN
F 3 "" H 7350 4150 50  0001 C CNN
	1    7350 4150
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0106
U 1 1 612B35D6
P 7350 3150
F 0 "#PWR0106" H 7350 3000 50  0001 C CNN
F 1 "VCC" H 7367 3323 50  0000 C CNN
F 2 "" H 7350 3150 50  0001 C CNN
F 3 "" H 7350 3150 50  0001 C CNN
	1    7350 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 3650 7350 3700
Wire Wire Line
	7350 4050 7350 4150
Wire Wire Line
	7350 3700 7000 3700
Connection ~ 7350 3700
Wire Wire Line
	7350 3700 7350 3750
Wire Wire Line
	7350 3150 7350 3250
Text Label 7000 3700 0    50   ~ 0
pb2
$Comp
L Device:R R3
U 1 1 612B35E3
P 7350 3900
F 0 "R3" H 7420 3946 50  0000 L CNN
F 1 "10k" H 7420 3855 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7280 3900 50  0001 C CNN
F 3 "~" H 7350 3900 50  0001 C CNN
	1    7350 3900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW4
U 1 1 612B405E
P 8250 3450
F 0 "SW4" V 8204 3598 50  0000 L CNN
F 1 "SW_Push" V 8295 3598 50  0000 L CNN
F 2 "Button_Switch_SMD:SW_SPST_B3S-1000" H 8250 3650 50  0001 C CNN
F 3 "~" H 8250 3650 50  0001 C CNN
	1    8250 3450
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 612B4064
P 8250 4150
F 0 "#PWR0107" H 8250 3900 50  0001 C CNN
F 1 "GND" H 8255 3977 50  0000 C CNN
F 2 "" H 8250 4150 50  0001 C CNN
F 3 "" H 8250 4150 50  0001 C CNN
	1    8250 4150
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0108
U 1 1 612B406A
P 8250 3150
F 0 "#PWR0108" H 8250 3000 50  0001 C CNN
F 1 "VCC" H 8267 3323 50  0000 C CNN
F 2 "" H 8250 3150 50  0001 C CNN
F 3 "" H 8250 3150 50  0001 C CNN
	1    8250 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 3650 8250 3700
Wire Wire Line
	8250 4050 8250 4150
Wire Wire Line
	8250 3700 7900 3700
Connection ~ 8250 3700
Wire Wire Line
	8250 3700 8250 3750
Wire Wire Line
	8250 3150 8250 3250
Text Label 7900 3700 0    50   ~ 0
pb3
$Comp
L Device:R R4
U 1 1 612B4077
P 8250 3900
F 0 "R4" H 8320 3946 50  0000 L CNN
F 1 "10k" H 8320 3855 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8180 3900 50  0001 C CNN
F 3 "~" H 8250 3900 50  0001 C CNN
	1    8250 3900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW5
U 1 1 612B5130
P 9150 3450
F 0 "SW5" V 9104 3598 50  0000 L CNN
F 1 "SW_Push" V 9195 3598 50  0000 L CNN
F 2 "Button_Switch_SMD:SW_SPST_B3S-1000" H 9150 3650 50  0001 C CNN
F 3 "~" H 9150 3650 50  0001 C CNN
	1    9150 3450
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 612B5136
P 9150 4150
F 0 "#PWR0109" H 9150 3900 50  0001 C CNN
F 1 "GND" H 9155 3977 50  0000 C CNN
F 2 "" H 9150 4150 50  0001 C CNN
F 3 "" H 9150 4150 50  0001 C CNN
	1    9150 4150
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0110
U 1 1 612B513C
P 9150 3150
F 0 "#PWR0110" H 9150 3000 50  0001 C CNN
F 1 "VCC" H 9167 3323 50  0000 C CNN
F 2 "" H 9150 3150 50  0001 C CNN
F 3 "" H 9150 3150 50  0001 C CNN
	1    9150 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 3650 9150 3700
Wire Wire Line
	9150 4050 9150 4150
Wire Wire Line
	9150 3700 8800 3700
Connection ~ 9150 3700
Wire Wire Line
	9150 3700 9150 3750
Wire Wire Line
	9150 3150 9150 3250
Text Label 8800 3700 0    50   ~ 0
pb4
$Comp
L Device:R R5
U 1 1 612B5149
P 9150 3900
F 0 "R5" H 9220 3946 50  0000 L CNN
F 1 "10k" H 9220 3855 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 9080 3900 50  0001 C CNN
F 3 "~" H 9150 3900 50  0001 C CNN
	1    9150 3900
	1    0    0    -1  
$EndComp
$Comp
L LED:SK6812MINI D1
U 1 1 612C0F99
P 3400 4750
F 0 "D1" H 3744 4796 50  0000 L CNN
F 1 "SK6812MINI" H 3744 4705 50  0000 L CNN
F 2 "LED_SMD:LED_SK6812MINI_PLCC4_3.5x3.5mm_P1.75mm" H 3450 4450 50  0001 L TNN
F 3 "https://cdn-shop.adafruit.com/product-files/2686/SK6812MINI_REV.01-1-2.pdf" H 3500 4375 50  0001 L TNN
	1    3400 4750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 612C18B7
P 3400 5050
F 0 "#PWR0111" H 3400 4800 50  0001 C CNN
F 1 "GND" H 3405 4877 50  0000 C CNN
F 2 "" H 3400 5050 50  0001 C CNN
F 3 "" H 3400 5050 50  0001 C CNN
	1    3400 5050
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0112
U 1 1 612C1E52
P 3400 4450
F 0 "#PWR0112" H 3400 4300 50  0001 C CNN
F 1 "VCC" H 3417 4623 50  0000 C CNN
F 2 "" H 3400 4450 50  0001 C CNN
F 3 "" H 3400 4450 50  0001 C CNN
	1    3400 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 4750 2800 4750
Text Label 2800 4750 0    50   ~ 0
status
$Comp
L Connector:Conn_01x06_Female J1
U 1 1 612D45EC
P 3400 2900
F 0 "J1" H 3292 3285 50  0000 C CNN
F 1 "Conn_01x06_Female" H 3292 3194 50  0000 C CNN
F 2 "Connector_JST:JST_EH_S6B-EH_1x06_P2.50mm_Horizontal" H 3400 2900 50  0001 C CNN
F 3 "~" H 3400 2900 50  0001 C CNN
	1    3400 2900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3600 2700 3850 2700
Text Label 3850 2800 2    50   ~ 0
pb1
Wire Wire Line
	3600 2800 3850 2800
Text Label 3850 2900 2    50   ~ 0
pb2
Wire Wire Line
	3600 2900 3850 2900
Text Label 3850 3000 2    50   ~ 0
pb3
Wire Wire Line
	3600 3000 3850 3000
Text Label 3850 3100 2    50   ~ 0
pb4
Wire Wire Line
	3600 3100 3850 3100
Text Label 3850 2700 2    50   ~ 0
pb0
Text Label 3850 3200 2    50   ~ 0
status
Wire Wire Line
	3600 3200 3850 3200
$EndSCHEMATC
